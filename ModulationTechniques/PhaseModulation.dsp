import("stdfaust.lib");

phase = os.osc(hslider("phase",0.3,0,20000,0.000001));
freq = hslider("freq",283,20,20000,0.000001);
uno = os.oscp(freq,phase);
due = os.oscp(freq,0);
process = uno*due : fi.dcblocker ;
